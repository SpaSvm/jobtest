from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from wishlist import views

router = DefaultRouter()
router.register(r'wishlist', views.WishListViewSet, basename='Wishlist')

urlpatterns = [
    url(r'^', include(router.urls)),
]
