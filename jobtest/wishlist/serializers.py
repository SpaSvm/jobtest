from rest_framework import serializers
from wishlist.models import Wish


class WishSerializer(serializers.ModelSerializer):
    total_price = serializers.FloatField(required=False)

    class Meta:
        model = Wish
        fields = [
            'title',
            'link',
            'completed',
            'price',
            'total_price'
        ]
