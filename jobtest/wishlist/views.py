from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from wishlist.serializers import WishSerializer
from wishlist.models import Wish, get_total_price
from django.db.models import Sum, FloatField, Value


class WishListViewSet(viewsets.ModelViewSet):
    serializer_class = WishSerializer
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    filterset_fields = ['completed']

    # def perform_create(self, serializer):
    #     serializer.save(user=self.request.user)

    def get_queryset(self):
        queryset = Wish.objects.filter(user=self.request.user)
        total_price = get_total_price(queryset)

        return queryset.annotate(total_price=Value(total_price, output_field=FloatField()))
