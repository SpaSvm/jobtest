from django.db import models

class Wish(models.Model):
    user = models.ForeignKey('auth.User', related_name='wishes', on_delete=models.CASCADE)
    title = models.CharField(max_length=250)
    link = models.URLField(blank=True)
    completed = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=11, decimal_places=2)


def get_total_price(queryset: list) -> float:
    total_price = 0

    for record in queryset.values():
        for element in record.keys():
            if element in 'price':
                total_price += record[element]

    return total_price
