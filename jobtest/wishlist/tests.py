from django.test import TestCase
from rest_framework.test import APIRequestFactory

factory = APIRequestFactory()
request = factory.post('/wishlist/', {
    'title': 'bicycle',
    'link': 'https://www.velopiter.ru/stels/pilot-350-z011/2018/',
    'completed': False,
    'price': '11400'
    }, format='json')
